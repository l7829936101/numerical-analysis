import numpy as np
import scipy.special as sc
from scipy import integrate


def simpson(f, a, b, m):
    """
    Integrate a function f from a to b using Simpson's method.
    """
    h = (b - a) / (2 * m)
    s = f(a) + f(b)
    for i in range(1, 2 * m, 2):
        s += 4 * f(a + i * h)
    for i in range(2, 2 * m, 2):
        s += 2 * f(a + i * h)
    return s * h / 3


def trapezoid(f, a, b, n):
    """
    Integrate a function f from a to b using trapezoid method.
    """
    h = (b - a) / n
    s = f(a) + f(b)
    for i in range(1, n):
        s += 2 * f(a + i * h)
    return s * h / 2


def iterate(f, a, b, epsilon):
    """
    Integrate a function f from a to b using iterative method.
    """
    h = b - a
    s = f(a) + f(b) * h / 2
    n = 1
    while True:
        h = h / 2
        n = n * 2

        s_old = s
        s = s / 2
        for i in range(1, n, 2):
            s += f(a + i * h) * h

        if abs(s - s_old) / abs(s) < epsilon:
            break
    return s


def romberg(f, a, b, epsilon):
    """
    Integrate a function f from a to b using Romberg's method.
    """

    def generate_tm():
        """
        Integrate a function f from a to b using iterative rule.
        """
        h = b - a
        s = f(a) + f(b) * h / 2
        yield s
        n = 1
        while True:
            h = h / 2
            n = n * 2

            s_old = s
            s = s / 2
            for i in range(1, n, 2):
                s += f(a + i * h) * h

            yield s

    generater = generate_tm()

    T = np.zeros((4, 4))
    for m in range(4):
        T[m, 0] = next(generater)
        base = 1
        for j in range(1, m + 1):
            base *= 4
            T[m - j, j] = (base * T[m - j + 1, j - 1] - T[m - j, j - 1]) / (base - 1)

    res = T[0, 3]
    m = 4
    while True:
        m += 1
        T[m % 4, 0] = next(generater)
        base = 1
        for j in range(1, 4):
            base *= 4
            T[(m - j) % 4, j] = (base * T[(m - j + 1) % 4, j - 1] - T[(m - j) % 4, j - 1]) / (base - 1)

        res_old = res
        res = T[(m + 1) % 4, 3]
        if abs(res - res_old) / abs(res) < epsilon:
            break
    return res


def gauss_legendre(f, a, b, n):
    """
    Integrate a function f from a to b using Gauss-Legendre's method.
    """
    root = sc.roots_legendre(n)[0]

    L_prime = sc.legendre(n).deriv(1)

    weight = [2 / ((1 - x ** 2) * (L_prime(x)) ** 2) for x in root]

    return sum([f((a + b) / 2 + (b - a) * x / 2) * w for x, w in zip(root, weight)]) * (b - a) / 2


def gauss_laguerre(f, n):
    """
    Integrate a function f from a to b using Gauss-Laguerre's method.
    """

    def f_(x):
        return f(x) * np.exp(x)

    root = sc.roots_laguerre(n)[0]

    U_prime = sc.laguerre(n).deriv(1)

    weight = 1 / (root * (U_prime(root) ** 2))

    return sum([f_(x) * w for x, w in zip(root, weight)])


def orthogonal_basic_func(rho, a, b, n):
    phi = [np.poly1d([1])]
    l2 = np.zeros(n + 1)
    l2[0] = integrate.quad(lambda x: (rho(x) * phi[0](x) * phi[0](x)), a, b)[0]
    for i in range(1, n + 1):
        coef = np.zeros(i)
        for j in range(i):
            coef[j] = -integrate.quad(lambda x: (rho(x) * phi[j](x) * (x ** i)), a, b)[0] / l2[j]

        p = np.poly1d([1] + [0] * i)

        for j in range(i):
            p += coef[j] * phi[j]

        phi.append(p)
        l2[i] = integrate.quad(lambda x: (rho(x) * phi[i](x) * phi[i](x)), a, b)[0]

    return phi


def gauss_rho(rho, a, b, n):
    """
    Integrate a function f from a to b using Gauss's method.
    """
    root = orthogonal_basic_func(rho, a, b, n)[-1].roots

    root = np.sort(root)

    weight = []
    for i in range(n):
        L = np.poly1d([1])
        for j in range(n):
            if j != i:
                L *= np.poly1d([1, -root[j]])
                L /= root[i] - root[j]

        weight.append(integrate.quad(lambda x: (rho(x) * L(x)), a, b)[0])

    return weight, root
