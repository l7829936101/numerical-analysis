import numpy as np

digits = 6
debug = False


def array2latex(array):
    if len(array.shape) == 1:
        return '(' + ','.join(map(str, array)) + ')'
    else:
        return '$$\n\\left[\\begin{array}{' + 'r' * (array.shape[1] - 1) + ':r}\n' + '\\\\\n'.join(
            [' & '.join(map(str, row)) for row in array]) + '\\end{array}\\right]\n$$'


def gauss_elimination(matrix, b):
    matrix_ = np.concatenate([matrix, b], axis=1)
    for k in range(matrix_.shape[0]):
        # find pivot
        i_max = np.argmax(np.abs(matrix_[k:, k])) + k
        # swap rows
        matrix_[[k, i_max]] = matrix_[[i_max, k]]
        # Make all rows below a_kk 0 in current column
        for i in range(k + 1, matrix_.shape[0]):
            c = (matrix_[i, k] / matrix_[k, k]).round(decimals=digits)
            matrix_[i] -= (c * matrix_[k]).round(decimals=digits)
            matrix_[i, k] = 0
        matrix_ = matrix_.round(decimals=digits)
        if debug:
            print(array2latex(matrix_))

    for k in range(matrix_.shape[0] - 1, -1, -1):
        matrix_[k, -1] /= matrix_[k, k]
        matrix_[k, k] = 1
        # Make all rows above a_kk 0 in current column
        for i in range(k):
            c = matrix_[i, k]
            matrix_[i] -= (c * matrix_[k]).round(decimals=digits)
            matrix_[i, k] = 0
        matrix_ = matrix_.round(decimals=digits)
        if debug:
            print(array2latex(matrix_))
    if debug:
        print(array2latex(matrix_[:, -1]))
    return matrix_[:, -1]


def doolittle(matrix, pivot_rule=False):
    matrix_ = matrix.copy()
    # Doolittle algorithm
    l = np.zeros((matrix_.shape[0], matrix_.shape[0]))
    u = np.zeros((matrix_.shape[0], matrix_.shape[0]))
    b = matrix_[:, -1].copy()
    for k in range(matrix_.shape[0]):
        if pivot_rule:
            # find pivot
            i_max = np.argmax(np.abs(ma[k:, k])) + k
            # swap rows
            matrix_[[k, i_max]] = matrix_[[i_max, k]]
            l[[k, i_max]] = l[[i_max, k]]
            b[[k, i_max]] = b[[i_max, k]]

        l[k, k] = 1
        u[k, k] = matrix_[k, k]

        for i in range(k + 1, matrix_.shape[0]):
            l[i, k] = matrix_[i, k] / matrix_[k, k]
            u[k, i] = matrix_[k, i]
            matrix_[i] -= l[i, k] * matrix_[k]
            matrix_[i, k] = 0

    if debug:
        print(array2latex(l.round(decimals=digits)))
        print(array2latex(u.round(decimals=digits)))

    y = np.zeros(matrix_.shape[0])
    x = np.zeros(matrix_.shape[0])
    for i in range(matrix_.shape[0]):
        y[i] = b[i]
        for j in range(i):
            y[i] -= l[i, j] * y[j]

    for i in range(matrix_.shape[0] - 1, -1, -1):
        x[i] = y[i]
        for j in range(i + 1, matrix_.shape[0]):
            x[i] -= u[i, j] * x[j]
        x[i] /= u[i, i]
    if debug:
        print(array2latex(y.round(decimals=digits)))
        print(array2latex(x.round(decimals=digits)))
    return x


def pursue(matrix, b):
    n = matrix.shape[0]

    r = np.zeros(n)
    for i in range(1, n):
        r[i] = matrix[i, i - 1]
    p = np.zeros(n)
    q = np.zeros(n)
    p[0] = matrix[0, 0]
    q[0] = matrix[0, 1] / p[0]
    for i in range(1, n - 1):
        p[i] = matrix[i, i] - r[i] * q[i - 1]
        q[i] = matrix[i, i + 1] / p[i]
    p[-1] = matrix[-1, -2] - r[-1] * q[-2]

    if debug:
        print(array2latex(r.round(decimals=digits)))
        print(array2latex(p.round(decimals=digits)))
        print(array2latex(q.round(decimals=digits)))

    y = np.zeros(n)
    x = np.zeros(n)
    y[0] = b[0] / p[0]
    for i in range(1, n):
        y[i] = (b[i] - y[i - 1]) / p[i]

    x[-1] = y[-1]
    for i in range(n - 2, -1, -1):
        x[i] = y[i] - q[i] * x[i + 1]

    if debug:
        print(array2latex(y.round(decimals=digits)))
        print(array2latex(x.round(decimals=digits)))
    return x


def jacobi(matrix, b, x0, eps):
    if np.any(np.diag(matrix) == 0):
        raise ValueError("Diagonal elements of matrix must be non-zero")

    x = x0.copy()
    count = 0
    while True:
        count += 1
        x_old = x.copy()
        for i in range(matrix.shape[0]):
            x[i] = (b[i] - np.dot(matrix[i, :i], x_old[:i]) - np.dot(matrix[i, i + 1:], x[i + 1:])) / matrix[i, i]
        if debug:
            print('x_{}'.format(count) + '=' + array2latex(x.round(decimals=digits)))
        if np.max(np.abs(x - x_old)) < eps:
            break
    return x


def gauss_seidel(matrix, b, x0, eps):
    if np.any(np.diag(matrix) == 0):
        raise ValueError("Diagonal elements of matrix must be non-zero")

    x = x0.copy()
    count = 0
    while True:
        count += 1
        x_old = x.copy()
        for i in range(matrix.shape[0]):
            x[i] = (b[i] - np.dot(matrix[i, :i], x[:i]) - np.dot(matrix[i, i + 1:], x[i + 1:])) / matrix[i, i]
        if debug:
            print('x_{}'.format(count) + '=' + array2latex(x.round(decimals=digits)))
        if np.max(np.abs(x - x_old)) < eps:
            break
    return x


def sor(matrix, b, x0, eps, w):
    if np.any(np.diag(matrix) == 0):
        raise ValueError("Diagonal elements of matrix must be non-zero")

    x = x0.copy()
    count = 0
    while True:
        count += 1
        x_old = x.copy()
        for i in range(matrix.shape[0]):
            x[i] = (1 - w) * x[i] + w * (b[i] - np.dot(matrix[i, :i], x[:i]) - np.dot(matrix[i, i + 1:], x[i + 1:])) / \
                   matrix[i, i]
        if debug:
            print('x_{}'.format(count) + '=' + array2latex(x.round(decimals=digits)))
        if np.max(np.abs(x - x_old)) < eps:
            break
    return x
