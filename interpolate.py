import numpy as np
import sympy as sp

DEBUG = True


def lagrange_interpolate(x, y):
    n = len(x)
    v = sp.symbols('x')
    res = 0

    for k in range(n):
        p = 1
        for i in range(n):
            if i != k:
                p *= (v - x[i]) / (x[k] - x[i])
        res += y[k] * p

    return sp.sympify(sp.expand(res))


def lagrange_interpolate_binary(x, y, u):
    n = len(x)
    m = len(y)

    v_x = sp.symbols('x')
    v_y = sp.symbols('y')
    res = 0

    for k in range(n):
        for r in range(m):
            p = 1
            for t in range(n):
                if t != k:
                    p *= (v_x - x[t]) / (x[k] - x[t])

            for t in range(m):
                if t != r:
                    p *= (v_y - y[t]) / (y[r] - y[t])

            res += u[k][r] * p

    return sp.sympify(sp.expand(res))


def newton_interploate(x, y):
    delta = np.zeros((len(x), len(x)))
    delta[0] = y
    for k in range(1, len(x)):
        for i in range(len(x) - k):
            delta[k][i] = (delta[k - 1][i + 1] - delta[k - 1][i]) / (x[i + k] - x[i])

    if DEBUG:
        print(delta)

    v = sp.symbols('x')
    res = 0
    p = 1

    for i in range(len(x)):
        res += delta[i][0] * p
        p *= (v - x[i])

    return sp.sympify(sp.expand(res))



def hermite_interploate(x, y, indices, y_prime):
    n = len(x)
    m = len(indices)

    pn = newton_interploate(x, y)

    pn_prime = pn.diff(sp.symbols('x'))

    qm = np.zeros(m)
    for k in range(m):
        qm[k] = y_prime[k] - pn_prime.subs(sp.symbols('x'), x[indices[k]])
        for j in range(n):
            if j != indices[k]:
                qm[k] /= (x[indices[k]] - x[j])

    qm = newton_interploate(x[indices], qm)

    v = sp.symbols('x')
    wn = 1
    for i in range(n):
        wn = wn * (v - x[i])

    res = pn + wn * qm
    return sp.sympify(sp.expand(res))


def m3_interpolate(x, y, u, v):
    n = len(x) - 1
    A = np.eye(n + 1) * 2
    b = np.zeros(n + 1)

    h = np.diff(x)
    h = np.insert(h, 0, 0)

    A[0, 1] = 1
    A[n, n - 1] = 1

    b[0] = 6 * ((y[1] - y[0]) / h[1] - u) / h[1]
    b[n] = 6 * (v - (y[n] - y[n - 1])) / h[n]

    for i in range(1, n):
        alpha_i = h[i + 1] / (h[i] + h[i + 1])
        A[i][i + 1] = alpha_i
        A[i][i - 1] = 1 - alpha_i

        b[i] = 6 * ((y[i + 1] - y[i]) / h[i + 1] - (y[i] - y[i - 1]) / h[i]) / (h[i] + h[i + 1])

    M = np.linalg.solve(A, b)

    var = sym.symbols('x')
    res = []
    for i in range(1, n + 1):
        s = M[i - 1] * (x[i] - var) ** 3 / (6 * h[i]) \
            + M[i] * (var - x[i - 1]) ** 3 / (6 * h[i]) \
            + (y[i - 1] / h[i] - M[i - 1] * h[i] / 6) * (x[i] - var) \
            + (y[i] / h[i] - M[i] * h[i] / 6) * (var - x[i - 1])

        res.append(s)
    return res


def function_approximation(a: int, b: int, f: sym.Function, phi: [sym.Function], Orthogonality=False):
    n = len(phi)
    G = np.zeros((n, n))
    bias = np.zeros(n)

    for i in range(n):
        bias[i] = sym.integrate(phi[i] * f, (sym.symbols('x'), a, b))
        G[i][i] = sym.integrate(phi[i] * phi[i], (sym.symbols('x'), a, b))

    if not Orthogonality:
        for i in range(n):
            for j in range(i):
                G[i][j] = sym.integrate(phi[i] * phi[j], (sym.symbols('x'), a, b))
                G[j][i] = G[i][j]

    c = np.linalg.solve(G, bias)

    res = 0
    for i in range(n):
        res += c[i] * phi[i]
    return res


def legendre_approximation(a: int, b: int, f: sym.Function, n: int):
    v = sym.symbols('x')
    phi = [sym.legendre(i, v) for i in range(n + 1)]

    f = f.subs(sym.symbols('x'), (a + b) / 2 + (b - a) / 2 * v)

    c = np.array([(2 * i + 1) / 2 * sym.integrate(phi[i] * f, (v, -1, 1)) for i in range(n + 1)])

    res = 0
    for i in range(n + 1):
        res += c[i] * phi[i]

    return sym.sympify(sym.expand(res.subs(sym.symbols('x'), (2 * v - (a + b)) / (b - a))))


def chebyshev_approximation(a: int, b: int, f: sym.Function, n: int):
    v = sym.symbols('x')
    phi = [1, v]
    for i in range(2, n + 1):
        phi.append(sym.simplify(sym.expand(2 * v * phi[i - 1] - phi[i - 2])))

    f = f.subs(sym.symbols('x'), (a + b) / 2 + (b - a) / 2 * v)

    c = np.zeros(n + 1)
    for i in range(n + 1):
        c[i] = 2 / np.pi * integrate.quad(lambda x: ((f * phi[i] / sym.sqrt(1 - v ** 2)).evalf(subs={'x': x})), -1, 1)[
            0]

    res = c[0] / 2
    for i in range(1, n + 1):
        res += c[i] * phi[i]

    return sym.sympify(sym.expand(res.subs(sym.symbols('x'), (2 * v - (a + b)) / (b - a))))

