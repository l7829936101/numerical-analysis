import numpy as np
import sympy as sp

def bisearch(a, b, f, eps, tol):
    if f(a) * f(b) > 0:
        return None
    while abs(b - a) > tol:
        c = (a + b) / 2
        if abs(f(c)) < eps:
            return c
        if f(c) * f(a) < 0:
            b = c
        else:
            a = c
    return (a + b) / 2


def simple_iter(x, f, tol):
    x_new = f(x)
    while (np.max(np.abs(x_new - x)) / np.max(np.abs(x_new))) > tol:
        x = x_new
        x_new = f(x)
    return x_new


def steffensen_iter(x, f, tol):
    y = f(x)
    z = f(y)
    x_new = x - (y - x) * (y - x) / (z - 2 * y + x)
    while abs((x_new - x) / x_new) > tol:
        x = x_new
        y = f(x)
        z = f(y)
        x_new = x - (y - x) * (y - x) / (z - 2 * y + x)
    return x_new


def newton_iter(x, f, tol):
    x_new = x - f(x) / f.deriv(x)
    while abs((x_new - x) / x_new) > tol:
        x = x_new
        x_new = x - f(x) / f.deriv(x)
    return x_new


def newton_iter_double(x, x_, f, tol):
    x_new = x - (f(x) * (x - x_)) / (f(x) - f(x_))
    while abs((x_new - x) / x_new) > tol:
        x_ = x
        x = x_new
        x_new = x - (f(x) * (x - x_)) / (f(x) - f(x_))
    return x_new


def newton_iter_single(x, x0, f, tol):
    x_new = x - (f(x) * (x - x0)) / (f(x) - f(x0))
    while abs((x_new - x) / x_new) > tol:
        x = x_new
        x_new = x - (f(x) * (x - x0)) / (f(x) - f(x0))
    return x_new


def newton_iter_array(x, f, f_, tol):
    x_new = x - np.matmul(np.linalg.inv(f_(x)), f(x))
    while (np.max(np.abs(x_new - x)) / np.max(np.abs(x_new))) > tol:
        x = x_new
        x_new = x - np.matmul(np.linalg.inv(f_(x)), f(x))
    return x_new


print(newton_iter_array(
    x=np.ones(2),
    f=lambda x: np.array([x[0] - np.sin(x[0] + x[1]) - 1.2, x[1] + np.cos(x[0] + x[1]) - 0.5]),
    f_=lambda x: np.array(
        [[1 - np.cos(x[0] + x[1]), -np.cos(x[0] + x[1])],
         [-np.sin(x[0] + x[1]), 1 - np.sin(x[0] + x[1])]]),
    tol=1e-4
))
